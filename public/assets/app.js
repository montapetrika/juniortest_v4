console.log("Hello World!");
//just shows the body element
window.addEventListener("load", function () {
    console.log(document.getElementsByTagName("body")[0])
});
/*
//adds background colour
window.addEventListener("load", function () {
    console.log(document.getElementsByTagName("body")[0].style.backgroundColor = "red");
})
*/

//jQuery

$(window).ready(function() {
    $("body").css("background", "blue");
});


