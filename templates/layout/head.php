<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name='author' content='Monta Petrika'/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Junior Developer Test Task for Scandiweb</title>
    <!--    <link rel="stylesheet" type="text/css" href="192.168.33.10/var/www/public/assets/stylesheet.css"> -->
    <!--    <link rel="stylesheet" type="text/css" href="../public/assets/stylesheet.css"> -->
    <link rel="stylesheet" href="/assets/stylesheet.css">
    <!-- phpStorm protests, but this is the only version that works! -->
    <!-- inspection regime on browser - hover over href, and it will show what it considers to be the full path  -->
    <!-- remember! dir 'public' is already mounted as IP default state; browser thinks it is index.php all the time  -->
    <!-- from the standpoint of INDEX.php -->
    <!--   <script src="/assets/app.js"></script>
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
</head>