<?php
//not used yet; will be returned in foreach, class that returns corresponding class depending on productType and/or View straight away?
//need to use classes to extract methods getLength(), for example, and then pass it to View...
// view(accepts abstract/interface Product $object/$class), returns/include_once the file which contains the correct type in name - concatenation

//in foreach call for "factory" class returnViewForProductType() that is given the record parameter (gives to class in return?)

//so code for checkbox and other stuff would not have to be repeated in every view - take out parent construct parent::__construct() from extended classes? ?????? not necessarily.
    //item-view-product needed with the similar part;
//first create new instance of parent class, then create this (with "factory" class) passing the same parameter???????

//part of products-list.php, of bigger form element, so variable names are the same as in that file
//class name = $item, whole graphic variable = $list, that contains all items at once
// if THIS was allowed to be a CLASS, then there would be no need to coordinate those variable names with other files...

$list .= '<div class="product-item">';
$list .= '<input class="delete-checkbox" name="delete_checkbox[]" type="checkbox" value="' . $item->productId() . '">';
$list .= '<p>' . $item->sku() . '</p>';
$list .= '<p>' . $item->productName() . '</p>';
$list .= '<p>' . $item->price() . DOLLARS . '</p>';
if (!empty ($item->weight())) {
    $list .= '<p>Weight: ' . $item->weight() . KILOGRAMS . '</p>';
}
$list .= '</div>';