<?php

//this if kind of a 'controller' file
if (($_SERVER['REQUEST_METHOD'] == 'POST') && (isset($_REQUEST['delete_button']))) {
    $manipulation = new DataManipulation(new DatabaseConnection);
    try {
        $manipulation->deleteProducts();
    } catch (Exception $e) {
        throw New Exception("Can't delete the products! \n", 0, $e);
    }
}
/**
 * Careful with changes! Variable names $list and $item are used in layout/item-wiew-(productType) files.
 */
try {
    //   echo 'Check!!!'."<br>";
    $listedProducts = new ListedProducts(new DatabaseConnection);
    $listedProductsArray = $listedProducts->allProductsArray();
    $productType = new ProductType;

    //    $list = '<div class="product-list-container">';
    $list = '<form id="product_list" name="product_list" method="post" action=" "> ';           // action="'. $_SERVER['PHP_SELF'].'"> '; action="{$_SERVER['PHP_SELF']}" - safer version //action="<?php // echo $_SERVER['PHP_SELF'];...">

    foreach ($listedProductsArray as $row) //->fetchAll() for PDO statement?
    {
        $item = $productType::getProductClass($row);    //static function, can be called like this
        include "layout/item-view-{$productType::getProductType($row)}.php";
//{} work only with double quotes; //include_once only includes the first instance of each type

        //could have separate item-view-product for the repeated part;
    }
    $list .= '</form>';
    //$list .= '</form></div>';
    //echo $list;

} catch (Exception $e) {
    throw new Exception("Can't select products from database", 0, $e);
}
?>

<?php require_once 'layout/head.php'; ?>

    <body>
    <header>
        <h1>Product List</h1>
        <!-- header buttons for Product List page -->
        <div class="btn-div">

            <a href="/products-add"> <!-- routes.php catches this URL -->
                <button type="button" id="lead-to-form-page-btn" class="btn" value="add">ADD</button>
            </a>
            <input type="submit" form="product_list" name="delete_button" id="delete-product-btn" class="btn"
                   value="MASS DELETE"/>
            <!--      <button type="submit" name="delete_button" form="product_list" id="delete-product-btn" class="btn" action="functions.php" value="delete">MASS DELETE</button>   -->
        </div>
    </header>

    <main>

        <!-- php // $_SERVER['DOCUMENT_ROOT']; ///var/www/public (index.php) in browser  -->
        <?= $list; ?>

    </main>

<?php require_once 'layout/footer.php'; ?>

<?php
