<?php require_once 'layout/head.php';

/**
 * this if kind of a 'controller' file
 * TODO session save posted data, autofill inputs (at least first 3; type switcher also?? should show hidden fieldset)
 * TODO what's up with those 3 forms on top of each other???
 */

if (($_SERVER['REQUEST_METHOD'] == 'POST') && (isset($_REQUEST['save_button']))) {
//TODO this validation (skuExists (false=good) isSkuUnique (true=good)) should go in separate validation class method
    $skuValues = new ListedProducts(new DatabaseConnection());
    $uniqueSkuArray = $skuValues->getAllUniqueValues('sku');
    if (!in_array(strtoupper($_POST['sku']), $uniqueSkuArray)) {        //CASE SENSITIVE comparison; otherwise input could be sent using different case usage, causing DB side error (UNIQUE still catches)
        $manipulation = new DataManipulation(new DatabaseConnection);
        $productType = new ProductType;
        try{
            $item = $productType::getProductClass($_POST);
            $manipulation->addProduct($item);   //argument accepts Product instance only, uses methods from it
        }catch(Exception $e){
            throw New Exception("Can't add a new product! \n", 0, $e);
        }
        header("Location: index.php");
    }
    else $msg = "* This SKU (".strtoupper($_POST['sku']).") already exists! No entry added!";
}

//all sku values for js to compare (need to turn into json):

$listedProducts = new ListedProducts(new DatabaseConnection);
$listOfSkuValues = strtoupper(json_encode($listedProducts->getAllUniqueValues('sku')));
//sudo apk add php7-json; sudo rc-service php-fpm7 restart //so json_encode would work
echo $listOfSkuValues;
?>
<script>
    function showProductParameters(id) {
        let selectedProductType = document.getElementById(id).value;      //fieldset's id === values of the options
        for (let option of document.getElementById(id).options) {
            if (option.value) {    //!!!!! first value is "", getElementById with 'falsy value' does not work; script stops working at that point without this line
                if (option.value == selectedProductType) {
                    document.getElementById(selectedProductType).style.display = 'block';
                    document.getElementById(selectedProductType).disabled = false;

                } else {
                    document.getElementById(option.value).style.display = 'none';
                    document.getElementById(option.value).disabled = true;   //disable fieldset from sending data, not required anymore
               }
                // if-else can be written as one line:
                //     document.getElementById(option.value).style.display = option.value == selectedProductType? 'block' : 'none';
            }
        }
    }

            var skuArray = <?php echo $listOfSkuValues; ?>;
            function validateSku() {
                var inputText = document.getElementById("sku").value.toUpperCase();     //comparison is CASE SENSITIVE!!!
               // alert(skuArray + " " + inputText);
                if (skuArray.includes(inputText)) {
                    document.getElementById("msg").innerHTML = "* This SKU already exists!";
                }
                else document.getElementById("msg").innerHTML = "";
       //     if  php? (in_array( input text , $uniqueSkuArray))
      //        $msg = "* This SKU already exists!";              or foreach check return false (what would it do?) and msg; checking inarray in js?
      //or just pass skuarray (which is initialized beforehand with loading the form) - in separate script initializing file; SkuValues class
            }

//instead of script - depending on product type - include necessary .php/html file - no problem with unnecessarily filled data and +/-required inputs.
//TODO - ^ could this work as 3 forms? if-else/switch-case include depending on product type?
</script>

<body>
<header>
    <h1>Product Add</h1>

    <!--header buttons for Product Add page -->

    <!-- ANCHORS WILL CHANGE, controller will controll what to include! -->

    <div class="btn-div">
        <!--    <button type="submit" form="product_form" id="save-product-btn" class="btn" value="save" >Save</button>  -->
        <input type="submit" form="product_form" formaction=" " name="save_button" id="save-product-btn" class="btn"
               value="Save"/>
        <a href="/products-list">
            <button type="button" id="cancel-btn" class="btn" value="cancel">Cancel</button>
        </a>
    </div>
</header>
<!-- maxlength does not work for numbers -->
<!-- max and min work -->
<main>
    <div class="product-form">
        <form id="product_form" name="product_form" method="post" action="">
            <!-- for this version input names === column names in DB; a must-have -->
            <fieldset id="fieldset-main">
                <label for="sku">SKU</label>
                <input id="sku" name="sku" type="text" maxlength="100" required oninput="validateSku()">
                <p id="msg"><?php if(isset($msg)) echo $msg; ?></p>
                <!-- js innerHTML uses this tag as well -->
                <br><br>

                <label for="name">Name</label>
                <input id="name" name="product_name" type="text" maxlength="100" required><br><br>

                <label for="price">Price</label>
                <input id="price" name="price" type="number" step="0.01" min="0.01" max="99999999.99" required><br><br>
                <!-- type number only allows to use dot and does not react to comma or letters(unless they are in the previously used list); also has up-down arrows for incrementing -->
                <!-- step to allow decimal values, with two decimals in this case -->

                <label for="productType">Type Switcher:</label>
                <select id="productType" name="product_type" onchange="showProductParameters('productType')" required>
                    <option value="" selected>Type Switcher</option>
                    <option value="dvd">DVD</option>
                    <option value="book">Book</option>
                    <option value="furniture">Furniture</option>
                </select>
            </fieldset>
            <!-- "option IDs" a.k.a fieldset IDs that are responsible for those options;
            option value === fieldset id, so functions shouldn't be changed if other options&fieldsets ar added based on this version -->
            <fieldset id="dvd">
                <label for="size">Size (MB)</label>
                <input id="size" name="size" type="number" min="1" max="99999999999" value="" required><br>
                <p class="product-description">Please, provide size (in MB)! Use whole numbers. </p>
            </fieldset>
            <!-- !!! before number type, decimals where allowed and rounded automatically in the table; now only whole numbers are allowed (with step="0.1" etc would alllow to write in form, but round up/down upon sending-->
            <fieldset id="book">
                <label for="weight">Weight (KG)</label>
                <input id="weight" name="weight" type="number" min="1" max="99999999999" value="" required><br>
                <p class="product-description">Please, provide weight (in KG)! Use whole numbers. </p>
            </fieldset>

            <fieldset id="furniture">
                <label for="height">Height (CM)</label>
                <input id="height" name="height" type="number" min="1" max="10000" value="" required><br><br>
                <!-- 100m should be enough for furniture -->

                <label for="width">Width (CM)</label>
                <input id="width" name="width" type="number" min="1" max="10000" value="" required><br><br>

                <label for="length">Length (CM)</label>
                <input id="length" name="length" type="number" min="1" max="10000" value="" required><br>
                <p class="product-description">Please, provide requested dimensions (using centimeters) for the
                    furniture! <br/> Use whole numbers. </p>
            </fieldset>
        </form>
    </div>
</main>
<?php require_once 'layout/footer.php'; ?>
</body>
</html>
