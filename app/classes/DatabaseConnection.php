<?php

class DatabaseConnection //extends \PDO
{
    private $dsn = DB_DSN;
    private $username = DB_USERNAME;
    private $password = DB_PASSWORD;

    /**
     * @return PDO|void
     * @throws Exception
     */
    public function dbConnection()
    {
        try {
            $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
            $databaseConn = new PDO(
                $this->dsn,
                $this->username,
                $this->password,
                $options);

        } catch (Exception $exception) {
            if (DEBUG_MODE === true) {
                echo "<pre>" . var_export($exception, true) . "</pre>";
            } else {
                echo "Whoops, something went wrong!";
            }
            throw new Exception("Can't connect to database!", 0, $exception);

            file_put_contents("../logs/exceptions.log",
                $exception->getMessage() . "\n" .
                $exception->getTraceAsString() . "\n" .
                $exception->getPrevious() . "\n\n\n",
                FILE_APPEND);
        }

        if (!empty ($databaseConn)) {
            return $databaseConn;
        }
    }
}