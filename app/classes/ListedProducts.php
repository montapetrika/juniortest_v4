<?php

class ListedProducts
{
    private $dbCon;
    private $allProductsArray; //for unique values

    /**
     * @param DatabaseConnection $dbCon
     * @throws Exception
     */
    public function __construct(DatabaseConnection $dbCon) {
        $this->dbCon = $dbCon;
        $this->allProductsArray = $this->allProductsArray();
    }
//    private $allProductsArray;    //not sure if this is needed, because it will go through an iteration in templates?
    private $allSkuValues;

//    public $database = new Database;  //expression not allowed as default value

    /**
     * @return array
     * @throws Exception
     */
    public function allProductsArray(): array
    {
//PDOStatement::fetchAll automatically returns FETCH_BOTH aka all values two times (assoc naming and indexed)
       try {
            $allProductsArray = [];
            $productList = $this->dbCon->dbConnection()->query('SELECT * FROM products ORDER BY product_id ASC');
//$productList is object(PDOStatement)#3 (1) { ["queryString"]=> string(22) "SELECT * FROM products" }

 /* V1
            $numRows = $productList->rowCount();        //$productList is somehow not yet an array..., needs to be read into an array
            if($numRows > 0) {
                while ($row = $productList->fetch(PDO::FETCH_ASSOC)) {
                    $listArray[] = $row;
                }
            }
            return $listArray;  */

            foreach ($productList->fetchAll(PDO::FETCH_ASSOC) as $row) {
                $allProductsArray[] = $row;     //LIEKS SOLIS? just return productList then fetchAll() as rows is exactly what's needed for listing the items
            }
            return $allProductsArray;

        } catch (Exception $exception) {
            if (DEBUG_MODE === true) {
                echo "<pre>" . var_export($exception, true) . "</pre>";
            } else {
                echo "Whoops, something went wrong!";
            }
            throw new Exception("Problems with showing the contents of database, selecting products!", 0, $exception);
//probably don't need this; this goes somewhere else..
  /*          file_put_contents("../logs/exceptions.log",
                $exception->getMessage() . "\n" .
                $exception->getTraceAsString() . "\n" .
                $exception->getPrevious() . "\n\n\n",
                FILE_APPEND); */
        }
    }

// although PDO already has this kind of function!!!! [left from mysqli times]
   public function getAllUniqueValues($columnName) : array      //data about listed products... or could be a separate class
    {
        $productListArray = $this->allProductsArray;
        $uniqueValuesArray = [];
        foreach ($productListArray as $rowNr => $observation) {         //whole table as a row number => whole observation
            foreach ($observation as $colName => $cellValue) {          //each observation as column name => cell value
                if ($colName == $columnName) {      //argument, passed in controller file
                    if (!in_array($cellValue, $uniqueValuesArray))     //DB has UNIQUE constraint, so not really needed here, does not upload duplicates
                        $uniqueValuesArray[] = strtoupper($cellValue);
//js and php sku value comparison is CASE SENSITIVE - for sku validation (otherwise possible to get error from DB (UNIQUE still catches it as last resort)
//if database contains anything other than same case letters
                }
            }
        }
        return $uniqueValuesArray;
    }


//HOW COULD I POSSIBLY USE SETTERS FOR GETTING DATA FROM DB?!?!?!   //->construct and setter methods, so they could have several arguments passed...
    /*
        public function setSku($row) //that's what I need to use in foreach in template, so just LONGER - set each, then show each..
        {
            $this->sku = $row['sku'];
        }
       */
/*
 //now these are in separate classes
//set the values from this particular DB record/observation/row/item IN THESE item PROPERTIES
//$record = $row
    public function setListedProductsItemData($item)    //argument is a row (record about an item) from DB
    {
        $this->productId = $item['product_id'];
        $this->sku = $item['sku'];
        $this->productName = $item['product_name'];
        $this->price = $item['price'];
        $this->productType = $item['product_type'];
        $this->size = (!empty ($item['size'])) ? $item['size'] : null;
        $this->weight = (!empty ($item['weight'])) ? $item['weight'] : null;
        $this->height = (!empty ($item['height'])) ? $item['height'] : null;
        $this->width = (!empty ($item['width'])) ? $item['width'] : null;
        $this->length = (!empty ($item['length'])) ? $item['length'] : null;
    }


    public function productId()
    {
        return $this->productId;
    }

    public function sku()
    {
        return $this->sku;
    }

    public function productName()
    {
        return $this->productName;
    }

    public function price()
    {
        return $this->price;
    }

    public function productType()
    {
        return $this->productType;
    }

    public function size()
    {
        return $this->size;
    }

    public function weight()
    {
        return $this->weight;
    }

    public function height()
    {
        return $this->height;
    }

    public function width()
    {
        return $this->width;
    }

    public function length()
    {
        return $this->length;
    }
/*
    /*
    //COULD ADD 'SKU' IN CONTROLLER - here ABSTRACT FUNCTION THAT GETS UNIQUE VALUES FROM ANY COLUMN getAllUniqueValues() (no setter, or it would need to be in construct)
    public function setAllUniqueSku() //: array  //if with return(then not set)
    {  //dublicates can still be uploaded straight into DB with SQL through phpAdmin, though not through the form //to do - not show sku duplicates? (or if everything the same) / warning that there are several of these...
        $productListArray = $this->allProductsArray();
        $uniqueSkuArray = [];
        foreach ($productListArray as $row => $observation) {         //whole table as a row number => whole observation
            foreach ($observation as $colName => $cellValue) {          //each observation as column name => cell value
                if ($colName == "sku") {
                    if (!in_array($cellValue, $uniqueSkuArray))     //DB has UNIQUE constraint, so not really needed here, does not upload duplicates
                        $uniqueSkuArray[] = strtoupper($cellValue);         //js and php sku value comparison is CASE SENSITIVE - for sku validation (otherwise possible to get error from DB (UNIQUE still catches it as last resort)
//if database contains anything other than same case letters

                }
            }
        }
        $this->allSkuValues = $uniqueSkuArray;
        //    return $uniqueSkuArray;  //this is an array
    }

    public function allSkuValues()
    {
        return $this->allSkuValues;
        //this is NULL! (without a __construct())
//my theory - because there is no construct that would ask for setters to create the value when the class is initialized, so gets just an empty property
    }
    */
}