<?php

class DataManipulation      //or ProductMySqlRepository? (accuracy?)    //needs to be edited when new products show up...
{
    private $dbCon;

    /**
     * @param DatabaseConnection $dbCon
     */
    public function __construct(DatabaseConnection $dbCon)
    {
        $this->dbCon = $dbCon;
    }

    /**
     * @param Product $product
     * @return void
     * @throws Exception
     */
    public function addProduct(Product $product)    //new ProductType -> getProductClass($_POSTed data[]) as an argument, product classes have getter functions
    {
        try {
            $query = $this->dbCon->dbConnection()->prepare(
                'INSERT INTO products (sku, product_name, price, product_type, size, weight, height, width, length)
                VALUES (:sku, :product_name, :price, :product_type, :size, :weight, :height, :width, :length)' );
            $query->execute([
                ':sku' => $product->sku(),
                ':product_name' => $product->productName(), //product_name in DB, name in input
                ':price' => $product->price(),
                ':product_type' => $product->productType(), //product_type in DB, productType in input
                //polymorphic calls bellow      //TODO inputnames != column names => abstract setters!!! then list and add controllers deal with naming differences
                ':size' => method_exists($product, 'size') ? $product->size() : null, //mysqli asked for 'NULL', MariaDB SQL syntax wants this
                ':weight' => method_exists($product, 'weight') ? $product->weight() : null,
                ':height' => method_exists($product, 'height') ? $product->height() : null,
                ':width' => method_exists($product, 'width') ? $product->width() : null,
                ':length' => method_exists($product, 'length') ? $product->length() : null
            ]);
        } catch (PDOException $exception) {
            throw new Exception("Can't add new product! \n", 0, $exception);
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    public function deleteProducts()
    {
//    if ( ($_SERVER['REQUEST_METHOD'] == 'POST') && (isset($_POST['delete_button'])) ) { //so function could be checked stand-alone without initial errors (while undefined)-if added in script file
//    if ( ($_SERVER['REQUEST_METHOD'] == 'POST') && (isset($_REQUEST['delete_button'])) )
        try {
            foreach ($_POST['delete_checkbox'] as $val)   //value = id
            {
                $delIds[] = intval($val);
            }
            $delSql = implode(",", $delIds);
//        var_dump($delIds);  //just checking
            $sql = "DELETE FROM products WHERE product_id IN ($delSql)"; //or {delSql} ??
            $this->dbCon->dbConnection()->query($sql);        //query - data need to be escaped - TODO?
        }
        catch (PDOException $exception) {
            throw New Exception("Problem occurred while trying to delete checked products. \n", 0, $exception);
        }
// }
    }
}