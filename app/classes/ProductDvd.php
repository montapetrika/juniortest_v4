<?php

class ProductDvd extends Product
{
    private $size;

    /**
     * @param $item
     * TODO //HOW could those items without sufficient data be skipped on public side and shown only on developer side?? (logged)
     */
    public function __construct($item) {    //record from DB //HOW TO ADD ABSTRACT CONSTRUCT? //$item[$dvdParameter]; now controller is easier, abstract here - controller harder
        parent::__construct($item);
        $this->size = $item['size'];
    }

    public function size()
    {
        return $this->size;
    }
}


      //if we don't want errors, then this ... [but this version shouldn't exist in DB]
//$this->size = (!empty ($item['size'])) ? $item['size'] : null;

