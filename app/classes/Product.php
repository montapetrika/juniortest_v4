<?php

abstract class Product
{
    private $productId;
    private $sku;
    private $productName;
    private $price;
    private $productType;

    //not really setters and getters; a little bit hard coded; array associated indexes might need to show up in controller/script only (maybe) ???
    //should I have separate setter functions with abstract naming? here constructor is the setter straight away (abstract here -> controller file assigns values using column names
    public function __construct($item) {    //record from DB
        if (isset($item['product_id'])) {
            $this->productId = $item['product_id'];
        }
        $this->sku = $item['sku'];
        $this->productName = $item['product_name'];
        $this->price = $item['price'];
        $this->productType = $item['product_type'];
    }

    public function productId() :int
    {
        return $this->productId;
    }

    public function sku() :string
    {
        return $this->sku;
    }

    public function productName() :string
    {
        return $this->productName;
    }

    public function price()         //float does not show zeros x.00
    {
        return $this->price;
    }

    public function productType() :string
    {
        return $this->productType;
    }
}