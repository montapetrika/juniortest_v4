<?php

class Validation
{ /*case sensitivity?*/
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }
    public function isUnique(string $columnName, ListedProducts $listedProducts) {
        $comparableArray = $listedProducts->getAllUniqueValues($columnName);
        if (!in_array(($this->data[$columnName]), $comparableArray)) {
            return TRUE;
        }
        return FALSE;
        }


        /*and the rest of them...*/
}