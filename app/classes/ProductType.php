<?php
//class that processes data about product type and provides product classes according to that type
class ProductType
{
    /**
     * @param $item - DB entry (record)
     * @return mixed
     * @throws Exception
     */
    public static function getProductType($item)
    {
// gets the needed cell from DB - not abstract; so not open-closed? Although this does not have to change as long as the DB does not change...
// might pass the needed cell/column as an argument {$item['columnName']} to the method, then need to specify the column name in 'controller' file
        $type = $item['product_type'];
        if (!empty($type)) {
            return $type;
        }
        // otherwise fail
        throw new Exception('Non-existent product type!');  //can anything be chained?
    }

    /**
     * @param $item
     * @return mixed
     * @throws Exception
     */
    public static function getProductClass($item)
    {
        $type = $item['product_type'];
        // construct class name and check its existence; case-insensitive
        $class = 'Product' . $type;
        if (class_exists($class)) {
            // return a new Product object
            return new $class($item);           //what to pass in completely abstracted version, where every field needs to be passed separately in controller?
        }
        // otherwise fail
        throw new Exception('Non-existent product class!');  //can anything be chained?
    }
}
