<?php
/**
 * UNUSED FILE; both product-list and product-add uses the same Product classes-subclasses and their methods.
 * setters - for absolutely abstract refactorable classes;
 * possibly need to change ProductType->getProductClass($item) implementation for it to work (see V2, no add-delete logic in that folder)
 */
//declare(strict_types = 1);

//in some type of controller - function gets called and argument passed straight away:
// product->setSku($_POST['sku']); for example
//this is a 'REFACTORABLE MODEL' only

class ProductForm
{
    private $sku;
    private $productName;
    private $price;
    private $productType;
    private $size;
    private $weight;
    private $height;
    private $width;
    private $length;

    public function setSku(string $sku)
    {
        if(strlen($sku) === 0) {
            throw new Exception("Invalid SKU");
        }

        $this->sku = $sku;
    }

    public function sku(): string
    {
        return $this->sku;
    }

    public function setProductName(string $productName)
    {
        if(strlen($productName) === 0) {
            throw new Exception("Invalid product name");
        }
        $this->productName = $productName;
    }

    public function productName(): string
    {
        return $this->productName;
    }

    public function setPrice(float $price)
    {
        if(strlen($price) === 0) {
            throw new Exception("Invalid product price");
        }
        $this->price = $price;
    }

    public function price(): float
    {
        return $this->price;
    }

    public function setProductType (string $productType)
    {
        if(strlen($productType) === 0) {
            throw new Exception("Invalid product type"); //select option, pretty much impossible
        }
        $this->productType = $productType;
    }

    public function productType(): string
    {
        return $this->productType;
    }

    public function setSize(int $size)
    {
        if(strlen($size) === 0) {
            throw new Exception("Invalid product size");
        }
        $this->size = $size;
    }

    public function size(): int
    {
        return $this->size;
    }

    public function setWeight(int $weight)
    {
        if(strlen($weight) === 0) {
            throw new Exception("Invalid product weight");
        }
        $this->weight = $weight;
    }

    public function weight(): int
    {
        return $this->weight;
    }

    public function setHeight(int $height)
    {
        if(strlen($height) === 0) {
            throw new Exception("Invalid product height");
        }
        $this->height = $height;
    }

    public function height(): int
    {
        return $this->height;
    }

    public function setWidth(int $width)
    {
        if(strlen($width) === 0) {
            throw new Exception("Invalid product width");
        }
        $this->width = $width;
    }

    public function width(): int
    {
        return $this->width;
    }

    public function setLength(int $length)
    {
        if(strlen($length) === 0) {
            throw new Exception("Invalid product length");
        }
        $this->length = $length;
    }

    public function length(): int
    {
        return $this->length;
    }


    /*
    public function setEmail(string $email)
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            throw new Exception("Invalid article email");
        }

        $this->email = $email;
    }

    public function email(): string
    {
        return $this->email;
    }*/

}