<?php

class ProductBook extends Product
{
    private $weight;

    public function __construct($item) {    //record from DB
        parent::__construct($item);
        $this->weight = $item['weight'];
    }

    public function weight()
    {
        return $this->weight;
    }
}

      //if we don't want errors, then this ... [but this version shouldn't exist in DB]
//        $this->weight = (!empty ($item['weight'])) ? $item['weight'] : null;