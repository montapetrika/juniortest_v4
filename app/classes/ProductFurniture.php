<?php

class ProductFurniture extends Product
{
    private $height;
    private $width;
    private $length;

    public function __construct($item) {    //record from DB
        parent::__construct($item);
        $this->height = $item['height'];
        $this->width = $item['width'];
        $this->length = $item['length'];
    }

    public function height()
    {
        return $this->height;
    }

    public function width()
    {
        return $this->width;
    }

    public function length()
    {
        return $this->length;
    }
}


/*      //if we don't want errors, then this ... [but this version shouldn't exist in DB]
        $this->height = (!empty ($item['height'])) ? $item['height'] : null;
        $this->width = (!empty ($item['width'])) ? $item['width'] : null;
        $this->length = (!empty ($item['length'])) ? $item['length'] : null;
 */