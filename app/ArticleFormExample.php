<?php

declare(strict_types = 1);

class Article
{
    private $title;
    private $email;
    private $content;

    public function setTitle(string $title)
    {
        if(strlen($title) === 0) {
            throw new Exception("Invalid article title");
        }

        $this->title = $title;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function setContent(string $content)
    {
        if(strlen($content) === 0) {
            throw new Exception("Invalid article content");
        }

        $this->content = $content;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function setEmail(string $email)
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            throw new Exception("Invalid article email");
        }

        $this->email = $email;
    }

    public function email(): string
    {
        return $this->email;
    }
}