<?php

require_once "config/env.php";
require_once "config/db-config.php";
require_once "config/measurement-config.php";

if (DEBUG_MODE === true) {
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
}
/*
set_exception_handler(function (Exception $e) { //catch uncaught exceptions? //problems arise, when there is an ERROR, not exception...
    // set default timezone
    date_default_timezone_set('Europe/Riga');

    // get the current date & time
    $time = date('d.m.Y, g:i a e O');

    // format the exception message
    $message = "[{$time}] {$e->getMessage()}\n  
    {$e->getTraceAsString()}\n
    {$e->getPrevious()}\n\n\n";
    // append to the error log (3)
    error_log($message, 3, '../logs/exceptions.log');

    // show a user-friendly message
    echo 'Whoops, looks like something went wrong!';
});
*/
//try {     //does not work in try catch statement
    // CLASS AUTOLOADER
    spl_autoload_register('myAutoLoader');
    function myAutoLoader($className)
    {
        //from which point of view - index or templates(yells that template can't find classes - to show the list), the first place to need classes? //similar to css, I guess
        // does not work written from the bootstrap.php file's point of view ("/classes")
        $path = "../app/classes/";
        $extension = ".php";
        $fullPath = $path . $className . $extension;

        if (!file_exists($fullPath)) {
            return false;
        }

        include_once $fullPath;
    } /*
} catch (Exception $exception) {
    file_put_contents("../logs/exceptions.log",
        $exception->getMessage() . "\n" .
        $exception->getTraceAsString() . "\n" .
        $exception->getPrevious() . "\n\n\n",
        FILE_APPEND);
}*/

require_once 'routes.php';

//require_once "ProductForm.php";
